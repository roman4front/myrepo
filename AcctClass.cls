public with sharing class AcctClass {
	public static void ChangeAccountName (Account [] acct) {
		Integer count = 1;
		for (Account a : acct) {
			a.Name = 'Account' + count;
			count++;
		}
	}
}